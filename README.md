# docs

non-technical documentations for AlphaQZ project backend including:

- project structure
- each microservice diagrams
- microservice interactions diagrams (grpc protos)